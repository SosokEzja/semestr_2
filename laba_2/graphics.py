import matplotlib.pyplot as plt


def count(elements, count_linear, count_binary, count_naive, count_kms):
    fig, ax = plt.subplots(1, 2)
    fig.suptitle('Оценка сложности по количеству операций')
    ax[0].bar(elements, count_linear, label="Линейный", width=20, alpha=0.7)
    ax[0].bar(elements, count_binary, label="Бинарный", width=20, alpha=0.7)
    ax[0].grid(which='both')
    ax[0].set_xlabel('Размер массива (кол-во элементов)')
    ax[0].set_ylabel('Кол-во итераций')
    ax[0].legend(fontsize=8, ncol=2, facecolor='white', edgecolor='black', title='Сортировки', title_fontsize='8')

    ax[1].bar(elements, count_naive, color="red", label="Наивный", width=20, alpha=0.7)
    ax[1].bar(elements, count_kms, color="green", label="Кнута-Морриса-Пратта", width=20, alpha=0.7)
    ax[1].grid(which='both')
    ax[1].set_xlabel('Размер массива (кол-во элементов)')
    ax[1].set_ylabel('Кол-во итераций')
    ax[1].legend(fontsize=8, ncol=2, facecolor='white', edgecolor='black', title='Сортировки', title_fontsize='8')
    plt.show()
