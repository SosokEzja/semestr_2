from Sort import bubble, insertion, shell, quick, func, points
from graphics import graphic

print("---Выберите пункты 1-5---\n"
      "[1]Пузырьковая сортировка\n"
      "[2]Сортировка вставками\n"
      "[3]Сортировка Шелла\n"
      "[4]Быстрая сортировка\n"
      "[5]Оценка сложности")

num = int(input(">>> "))

if num == 1:
    print("Пузырьковая сортировка")
    size = int(input("Введите размер массива: "))
    unsorted_mass = func(size)
    print(f'Неотсортированный массив: {unsorted_mass}')
    mass, time = bubble(unsorted_mass)
    print(f'Отсортированный массив: {mass}\nВремя работы: {time} (с)')

elif num == 2:
    print("Сортировка вставками")
    size = int(input("Введите размер массива: "))
    unsorted_mass = func(size)
    print(f'Неотсортированный массив: {unsorted_mass}')
    mass, time = insertion(unsorted_mass)
    print(f'Отсортированный массив: {mass}\nВремя работы: {time} (с)')

elif num == 3:
    print("Сортировка Шелла")
    size = int(input("Введите размер массива: "))
    unsorted_mass = func(size)
    print(f'Неотсортированный массив: {unsorted_mass}')
    mass, time = shell(unsorted_mass)
    print(f'Отсортированный массив: {mass}\nВремя работы: {time} (с)')

elif num == 4:
    print("Быстрая сортировка")
    size = int(input("Введите размер массива: "))
    unsorted_mass = func(size)
    print(f'Неотсортированный массив: {unsorted_mass}')
    first = 0
    last = len(func(size)) - 1
    mass, time = quick(unsorted_mass, first, last)
    print(f'Отсортированный массив: {mass}\nВремя работы: {time} (с)')

elif num == 5:
    print("Оценка сложности")
    min_size = int(input("Введите минимальный размер массива:"))
    max_size = int(input("Введите максимальный размер массива:"))

    points = points(min_size, max_size)

    time_bubble, time_insertion, time_shell, time_quick = [], [], [], []

    for i in range(min_size, max_size + 10, 10):
        arr_bubble, time_b = bubble(func(i))
        time_bubble.append(time_b)

        arr_insertion, time_i = insertion(func(i))
        time_insertion.append(time_i)

        arr_shell, time_s = shell(func(i))
        time_shell.append(time_s)

        first = 0
        last = len(func(i)) - 1
        arr_quick, time_q = quick(func(i), first, last)
        time_quick.append(time_q)

    graphic(points, time_bubble, time_insertion, time_shell, time_quick)
